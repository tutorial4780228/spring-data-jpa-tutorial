package dev.maven.springdatajpatutorial.database.student

import dev.maven.springdatajpatutorial.database.student.Student
import jakarta.transaction.Transactional
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import java.util.*

interface StudentRepositoryInterface : JpaRepository<Student, Long> {
    fun findByFirstName(name: String): List<Student>
    fun findByFirstNameContaining(phrase: String): List<Student>
    fun findByGuardianName(guardianName: String): List<Student>
    fun findByFirstNameContainingAndLastNameContaining(firstNamePhrase: String, lastNamePhrase: String): Optional<Student>

    @Query("""
        select student 
        from Student student 
        where student.emailId = ?1
    """)
    fun getStudentByEmailAddress(emailAddress: String): Optional<Student>

    @Query("""
        select student.firstName, student.lastName
        from Student student 
        where student.emailId = :emailAddress
    """)
    fun getStudentNameByEmailAddress(emailAddress: String): Optional<String>

    @Query(nativeQuery = true, value = """
        SELECT *
        FROM tbl_student
        WHERE email = :emailAddress
    """)
    fun getStudentByEmailAddressNative(emailAddress: String): Optional<Student>

    @Modifying
    @Transactional
    @Query("""
        update Student student
        set student.firstName = :updatedName
        where student.emailId = :emailId
    """)
    fun updateStudentNameByEmailId(emailId: String, updatedName: String)
}