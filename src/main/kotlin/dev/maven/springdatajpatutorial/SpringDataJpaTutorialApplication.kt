package dev.maven.springdatajpatutorial

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringDataJpaTutorialApplication

fun main(args: Array<String>) {
    runApplication<SpringDataJpaTutorialApplication>(*args)
}
