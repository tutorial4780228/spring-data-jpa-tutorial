package dev.maven.springdatajpatutorial.database.student

import jakarta.persistence.AttributeOverride
import jakarta.persistence.AttributeOverrides
import jakarta.persistence.Column
import jakarta.persistence.Embeddable

@Embeddable
@AttributeOverrides(
    AttributeOverride(name = "name", column = Column(name = "guardian_name")),
    AttributeOverride(name = "email", column = Column(name = "guardian_email")),
    AttributeOverride(name = "mobile", column = Column(name = "guardian_mobile"))
)
data class Guardian (
    var name: String = "",
    var email: String = "",
    var mobile: String = ""
) {
}