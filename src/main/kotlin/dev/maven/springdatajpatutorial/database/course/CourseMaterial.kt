package dev.maven.springdatajpatutorial.database.course

import jakarta.persistence.*

@Entity
data class CourseMaterial (
    @Id
    @SequenceGenerator(
        name = "course_material_id_sequence_generator",
        sequenceName = "course_material_id_sequence",
        allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "course_material_id_sequence_generator")
    var courseMaterialId: Long = 0L,

    var url: String = "",

    @OneToOne(cascade = [CascadeType.ALL], optional = false)
    @JoinColumn(name = "course_id", referencedColumnName = "courseId")
    var course: Course? = null
) {
    override fun toString(): String {
        return "CourseMaterial(courseMaterialId=$courseMaterialId, url='$url')"
    }
}