package dev.maven.springdatajpatutorial

import dev.maven.springdatajpatutorial.database.course.Course
import dev.maven.springdatajpatutorial.database.course.CourseMaterial
import dev.maven.springdatajpatutorial.database.course.CourseRepositoryInterface
import dev.maven.springdatajpatutorial.database.student.Student
import dev.maven.springdatajpatutorial.database.teacher.Teacher
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort

@SpringBootTest
class CourseRepositoryTest (
    @Autowired private val courseRepository: CourseRepositoryInterface
) {
    @Test
    fun printCourses() {
        val courses = courseRepository.findAll()
        println(courses)
    }

    @Test
    fun findAllPagination() {
        val firstPageWithThreeRecords = PageRequest.of(0, 3)
        val secondPageWithTwoRecords = PageRequest.of(1, 2)

        val courses = courseRepository.findAll(firstPageWithThreeRecords).content
        val totalElements = courseRepository.findAll(firstPageWithThreeRecords).totalElements
        val totalPages = courseRepository.findAll(firstPageWithThreeRecords).totalPages

        println("$courses, $totalElements, $totalPages, $firstPageWithThreeRecords, $secondPageWithTwoRecords")
    }

    @Test
    fun findAllWithSorted() {
        val sortByTitle = PageRequest.of(
            0,
            2,
            Sort.by("title").and(Sort.by("credit"))
        )
        println(courseRepository.findAll(sortByTitle).content)
    }

    @Test
    fun saveCourseWithTeacher() {
        val course = Course(
            title = "Python",
            credit = 6,
            teacher = Teacher(firstName = "Priyanka", lastName = "Singh")
        )
        courseRepository.save(course)
    }

    @Test
    fun saveCourseWithStudentAndTeacher() {
        val course = Course(
            title = "AI",
            credit = 12,
            teacher = Teacher(
                firstName = "Lizzy",
                lastName = "Morgan"
            ),
            students = listOf(
                Student(
                    firstName = "Ahhishek",
                    lastName = "Singh",
                    emailId = "abhishek@gmail.com"
                )
            )
        )

        courseRepository.save(course)
    }
}