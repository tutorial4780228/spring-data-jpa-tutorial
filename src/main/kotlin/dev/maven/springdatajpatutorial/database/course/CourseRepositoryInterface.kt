package dev.maven.springdatajpatutorial.database.course

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CourseRepositoryInterface : JpaRepository<Course, Long>