package dev.maven.springdatajpatutorial.database.course

import dev.maven.springdatajpatutorial.database.student.Student
import dev.maven.springdatajpatutorial.database.teacher.Teacher
import jakarta.persistence.*

@Entity
data class Course (
    @Id
    @SequenceGenerator(name = "course_id_sequence_generator", sequenceName = "course_id_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "course_id_sequence_generator")
    var courseId: Long = 0L,

    var title: String = "",
    var credit: Int = 0,

    @OneToOne(mappedBy = "course", optional = false)
    var courseMaterial: CourseMaterial? = null,

    @ManyToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "teacher_id", referencedColumnName = "teacherId")
    var teacher: Teacher? = null,

    @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    @JoinTable(
        name = "enrollment",
        joinColumns = [JoinColumn(name = "course_id", referencedColumnName = "courseId")],
        inverseJoinColumns = [JoinColumn(name = "student_id", referencedColumnName = "studentId")]
    )
    var students: List<Student> = listOf()
)