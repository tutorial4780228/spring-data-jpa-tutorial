package dev.maven.springdatajpatutorial.database.teacher

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface TeacherRepositoryInterface : JpaRepository<Teacher, Long>