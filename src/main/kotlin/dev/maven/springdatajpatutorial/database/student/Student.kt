package dev.maven.springdatajpatutorial.database.student

import jakarta.persistence.*

@Entity
@Table(name = "tbl_student", uniqueConstraints = [
    UniqueConstraint(
        name = "unique_email",
        columnNames = ["email"]
    )
])
data class Student (
    @Id
    @SequenceGenerator(name = "student_id_sequence_generator", sequenceName = "student_id_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "student_id_sequence_generator")
    var studentId: Long = 0L,

    var firstName: String = "",
    var lastName: String = "",

    @Column(name = "email", nullable = false, )
    var emailId: String = "",

    @Embedded
    val guardian: Guardian? = null
)