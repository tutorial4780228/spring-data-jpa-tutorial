package dev.maven.springdatajpatutorial.database.teacher

import dev.maven.springdatajpatutorial.database.course.Course
import jakarta.persistence.*

@Entity
data class Teacher (
    @Id
    @SequenceGenerator(name = "teacher_sequence_generator", sequenceName = "teacher_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "teacher_sequence_generator")
    var teacherId: Long = 0L,

    var firstName: String = "",
    var lastName: String = "",

    @OneToMany(mappedBy = "teacher", cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    var courses: List<Course> = emptyList()
) {
    override fun toString(): String {
        return "Teacher(teacherId=$teacherId, firstName='$firstName', lastName='$lastName')"
    }
}