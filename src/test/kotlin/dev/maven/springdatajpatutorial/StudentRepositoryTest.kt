package dev.maven.springdatajpatutorial

import com.github.javafaker.Faker
import dev.maven.springdatajpatutorial.database.student.StudentRepositoryInterface
import dev.maven.springdatajpatutorial.database.student.Guardian
import dev.maven.springdatajpatutorial.database.student.Student
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class StudentRepositoryTest (
    @Autowired private val studentRepository: StudentRepositoryInterface,
) {
    private val faker: Faker = Faker()
    private var addedStudent: Student = Student()

    @Test
    fun saveStudent() {
        createStudent()
    }

    fun createStudent() {
        val student = Student(
            firstName = faker.name().firstName(),
            lastName = faker.name().lastName(),
            emailId = faker.internet().emailAddress(),
            guardian = Guardian(
                name = faker.name().firstName(),
                email = faker.internet().emailAddress(),
                mobile = faker.phoneNumber().cellPhone()
            )
        )
        addedStudent = student
        studentRepository.save(student)
    }

    @Test
    fun saveStudentWithGuardianDetails() {
        val student = Student(
            firstName = faker.name().firstName(),
            lastName = faker.name().lastName(),
            emailId = faker.internet().emailAddress(),
            guardian = Guardian(
                name = faker.name().firstName(),
                email = faker.internet().emailAddress(),
                mobile = faker.phoneNumber().cellPhone()
            )
        )
        addedStudent = student
        studentRepository.save(student)
    }

    @Test
    fun printAllStudents() {
        val studentList = studentRepository.findAll()
        println(studentList)
    }

    @Test
    fun printStudentWithFirstName() {
        createStudent()
        val students = studentRepository.findByFirstName(addedStudent.firstName)
        println(students)
    }

    @Test
    fun printStudentWithFirstNameContaining() {
        createStudent()
        val students = studentRepository.findByFirstNameContaining(addedStudent.firstName)
        println(students)
    }

    @Test
    fun printStudentWithGuardianName() {
        createStudent()
        val students = studentRepository.findByGuardianName(addedStudent.guardian!!.name)
        print(students)
    }

    @Test
    fun printStudentWithFirstNameContainingAndLastNameContaining() {
        createStudent()
        val student = studentRepository.findByFirstNameContainingAndLastNameContaining(
            addedStudent.firstName,
            addedStudent.lastName
        )

        if (student.isPresent) {
            println(student.get())
        }
    }

    @Test
    fun printStudentWithGivenEmail() {
        createStudent()
        val studentNameOptional = studentRepository.getStudentNameByEmailAddress(addedStudent.emailId)

        if (studentNameOptional.isPresent) {
            val fullNameUnformatted = studentNameOptional.get()
            val (firstName, lastName) = fullNameUnformatted.split(',')
            println("$firstName $lastName")
        }
    }

    @Test
    fun printStudentWithGivenEmailNativeQuery() {
        createStudent()
        val studentOptional = studentRepository.getStudentByEmailAddressNative(addedStudent.emailId)

        if (studentOptional.isPresent) {
            println(studentOptional.get())
        }
    }

    @Test
    fun updateStudentNameByEmailId() {
        createStudent()
        val emailId = addedStudent.emailId
        val updatedName = faker.name().firstName()
        studentRepository.updateStudentNameByEmailId(emailId, updatedName)

        val studentOptional = studentRepository.getStudentByEmailAddress(emailId)
        assert(studentOptional.isPresent)
        assert(studentOptional.get().firstName == updatedName)
    }

}