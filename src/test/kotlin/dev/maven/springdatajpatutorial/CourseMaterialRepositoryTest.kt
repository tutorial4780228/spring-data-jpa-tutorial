package dev.maven.springdatajpatutorial

import dev.maven.springdatajpatutorial.database.course.Course
import dev.maven.springdatajpatutorial.database.course.CourseMaterial
import dev.maven.springdatajpatutorial.database.course.CourseMaterialRepositoryInterface
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class CourseMaterialRepositoryTest (
    @Autowired private val courseMaterialRepository: CourseMaterialRepositoryInterface,
) {
    @Test
    fun saveCourseMaterial() {
        val courseMaterial = CourseMaterial(
            url = "https://www.google.com/course",
            course =  Course(
                title =  "DSA",
                credit = 6
            )
        )
        courseMaterialRepository.save(courseMaterial)
    }

    @Test
    fun printAllCourseMaterials() {
        val courseMaterials = courseMaterialRepository.findAll()

        courseMaterials.forEach {
            courseMaterial -> println(courseMaterial)
        }
    }
}