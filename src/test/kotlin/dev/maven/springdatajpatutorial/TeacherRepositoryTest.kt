package dev.maven.springdatajpatutorial

import dev.maven.springdatajpatutorial.database.course.Course
import dev.maven.springdatajpatutorial.database.teacher.Teacher
import dev.maven.springdatajpatutorial.database.teacher.TeacherRepositoryInterface
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class TeacherRepositoryTest (
    @Autowired private val teacherRepository: TeacherRepositoryInterface
) {
    @Test
    fun saveTeacher() {
        val teacher = Teacher(
            firstName = "Outub",
            lastName = "Khan",
//            courses = listOf(
//                Course(title = "DBA", credit = 5),
//                Course(title = "Java", credit = 6)
//            )
        )

        teacherRepository.save(teacher)
    }

    @Test
    fun getAllTeachers() {
        val teachers = teacherRepository.findAll()
        teachers.forEach{
            teacher -> println(teacher.courses)
        }
    }
}